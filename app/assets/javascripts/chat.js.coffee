jQuery ->
  window.chatController = new Chat.Controller($('#chat').data('uri'), true);

window.Chat = {}

class Chat.User
  constructor: (@user_name, @user_image) ->
  serialize: => { user_name: @user_name, user_image: @user_image }

class Chat.Controller
  template: (message) ->
    html =
      """
    <div class="mensaje-amigo">
    	<div class="contenido" style="background-color: #{message.user_color};">
    	    #{message.msg_body}
    	</div>
    	<img src="#{message.user_image}" alt="user" height="42" width="42">
    	<div class="fecha">Enviado #{message.received} #{message.user_name}</div>
    </div>
      """
    $(html)

  userListTemplate: (userList) ->
    userHtml = ""
    for user in userList
      userHtml = userHtml + "<li><img src='#{user.user_image}' alt='user' height='42' width='42'> #{user.user_name}</li>"
    $(userHtml)

  constructor: (url,useWebSockets) ->
    @messageQueue = []
    @dispatcher = new WebSocketRails(url,useWebSockets)
    @dispatcher.on_open = @createGuestUser
    @bindEvents()

  bindEvents: =>
    @dispatcher.bind 'new_message', @newMessage
    @dispatcher.bind 'user_list', @updateUserList
    $('#refresh').on 'click', @updateUserInfo
    # $('input#user_name').on 'keyup', @updateUserInfo
    $('input#storage_name').on 'keyup', @updateUserInfo
    $('input#storage_image').on 'keyup', @updateUserImage
    $('#send').on 'click', @sendMessage
    $('#message').keypress (e) -> $('#send').click() if e.keyCode == 13

  newMessage: (message) =>
    @messageQueue.push message
    @shiftMessageQueue() if @messageQueue.length > 15
    @appendMessage message

  sendMessage: (event) =>
    event.preventDefault()
    message = $('#message').val()
    @dispatcher.trigger 'new_message', {user_name: @user.user_name, msg_body: message}
    $('#message').val('')

  updateUserList: (userList) =>
    $('#user_list').html @userListTemplate(userList)

  updateUserInfo: (event) =>
    @user.user_name = $('input#storage_name').val()
    localStorage.setItem('username',$('input#storage_name').val())
    $('#user_name').html @user.user_name
    @dispatcher.trigger 'change_username', @user.serialize()

  updateUserImage: (event) =>
    @user.user_image = $('input#storage_image').val()
    localStorage.setItem('image',$('input#storage_image').val())
    $('#userimage').html @user.user_image
    @dispatcher.trigger 'change_image', @user.serialize()

  appendMessage: (message) ->
    messageTemplate = @template(message)
    $('#chat').append messageTemplate
    messageTemplate.slideDown 140

  shiftMessageQueue: =>
    @messageQueue.shift()
    $('#chat div.messages:first').slideDown 100, ->
      $(this).remove()

  createGuestUser: =>
    if localStorage.getItem('username')
      @user = new Chat.User(localStorage.getItem('username'), localStorage.getItem('image'))
      $('#user_image').html @user.user_image
      $('input#storage_image').val @user.user_image
      $('#user_name').html @user.user_name
      $('input#storage_name').val @user.user_name
      @dispatcher.trigger 'new_user', @user.serialize()
    else
      rand_num = Math.floor(Math.random()*1000)
      @user = new Chat.User("Guest_" + rand_num)
      @user.user_image = 'http://fc07.deviantart.net/fs71/i/2013/362/0/e/yasuo_by_njo_ku-d6zsqub.png'
      $('input#storage_image').val @user.user_image
      $('#user_name').html @user.user_name
      # $('input#user_name').val @user.user_name
      $('input#storage_name').val @user.user_name
      @dispatcher.trigger 'new_user', @user.serialize()