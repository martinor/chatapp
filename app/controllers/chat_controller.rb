class ChatController < WebsocketRails::BaseController
  include ActionView::Helpers::SanitizeHelper

  def initialize_session
    puts "Sesion iniciada\n"
  end
  
  def system_msg(ev, msg)
    broadcast_message ev, { 
      user_name: 'system',
      user_color: '#FF0000',
      user_image: 'http://fc07.deviantart.net/fs71/i/2013/362/0/e/yasuo_by_njo_ku-d6zsqub.png',
      received: Time.now.to_s(:short), 
      msg_body: msg
    }
  end
  
  def user_msg(ev, msg)
    broadcast_message ev, { 
      user_name:  connection_store[:user][:user_name],
      user_color:  connection_store[:color][:user_color],
      user_image: connection_store[:user][:user_image],
      received:   Time.now.to_s(:short),
      msg_body:   ERB::Util.html_escape(msg) 
    }
  end
  
  def client_connected
    system_msg :new_message, "client #{client_id} connected"
    broadcast_user_list
  end
  
  def new_message
    user_msg :new_message, message[:msg_body].dup
  end
  
  def new_user
    connection_store[:user] = { user_name: sanitize(message[:user_name]), user_image: sanitize(message[:user_image]) }
    connection_store[:color] = { user_color: "#"+"%06x" % (rand * 0xffffff) }
    broadcast_user_list
  end
  
  def change_username
    connection_store[:user][:user_name] = sanitize(message[:user_name])
    broadcast_user_list
  end

  def change_image
    connection_store[:user][:user_image] = sanitize(message[:user_image])
    broadcast_user_list
  end
  
  def delete_user
    connection_store[:user] = nil
    system_msg "client #{client_id} disconnected"
    broadcast_user_list
  end
  
  def broadcast_user_list
    users = connection_store.collect_all(:user)
    broadcast_message :user_list, users
  end
  
end